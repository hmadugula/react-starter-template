import React from "react";
import { Nav, NavItem, NavLink } from "reactstrap";

import Topbar from "../topbar/topbar.component.jsx";

class Main extends React.Component {
  constructor(props, history) {
    super(props);
    // console.log("props :: in builderModel", this.props)
    this.state = {
      activeTab: 1,
    };
  }
  setActiveTab(tabNumber) {
    this.setState({
      activeTab: tabNumber,
    });
  }
  checkIfActiveTab(tabNumber) {
    return this.state.activeTab === tabNumber ? "active" : "";
  }
  render() {
    return (
      <div className="">
        <Topbar />
        <div className="container-fluid">
          <section className="jumbotron text-center">
            <div className="container">
              <h1>Random example</h1>
              <p className="lead text-muted">
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>

              <p>
                <button className="btn btn-primary my-2">
                  Button 1
                </button>
                <button className="btn btn-primary ml-2 my-2">
                  Button 2
                </button>
                {/* <a >Button 1</a> */}
                {/* // <a className="btn btn-secondary ml-2 my-2">Secondary action</a> */}
              </p>
            </div>
          </section>
        </div>
      </div>
    );
  }
}

export default Main;
