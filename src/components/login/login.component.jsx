import React, { Component } from "react";
import { Button, Form, FormGroup, Label, Input, FormText } from "reactstrap";
import { Link } from "react-router-dom";

import "./login.component.css";
class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      valid: true,
    };
  }

  handleChange(event) {
    var inputName = event.target.name;
    this.setState({ [inputName]: event.target.value });
  }
  login = () => {
    console.log("valid value", this.state.valid);
    if (this.state.password == "Miracle@123") {
      this.setState({ valid: false });
      this.props.history.push({
        pathname: "/main/",
      });
    }
  };

  render() {
    return (
      <div className="container">
        <div className="text-center mt-5">
          <Form className="form-signin">
            <img className="mb-4" src="https://d2b8lqy494c4mo.cloudfront.net/mss/images/miracle-logo-01.svg" alt="" />
            {/* <h1 className="h3 mb-3 font-weight-normal">Please sign in</h1> */}
            {/* <label htmlFor="inputEmail" className="sr-only">Email address</label> */}
            <Input
              type="email"
              name="email"
              className="form-control mt-4"
              // value={this.state.email}
              id="exampleEmail"
              required
              placeholder="Email Address"
              onChange={(e) => {
                this.handleChange(e);
              }}
            />
            <label htmlFor="inputPassword" className="sr-only mt-2">Password</label>
            <Input
              type="password"
              name="password"
              className="form-control mt-2"
              // value={this.state.password}
              id="examplePassword"
              required
              placeholder="Password"
              onChange={(e) => {
                this.handleChange(e);
              }}
            />
            {/* <div className="checkbox mb-3">
              <label><input type="checkbox" value="remember-me" /> Remember me</label>
            </div> */}
            {!this.state.valid ?
              (
                <span style={{ color: "red" }}> Invalid Credentials</span>
              )
              : null}
            <button
              className="btn btn-lg btn-primary btn-block mt-3"
              onClick={() => {
                this.login();
              }}
            >
              Submit
          </button>
            {/* <button className="btn btn-lg btn-primary btn-block" type="submit">Sign in</button> */}
            <p className="mt-5 mb-3 text-muted">Miracle Labs © 2020</p>
          </Form>
        </div>

      </div>
    );
  }
}

export default Login;
