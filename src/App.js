import React from 'react';
import logo from './logo.svg';

import RouterOutlet from './components/routing.component.jsx'

import './App.scss';

function App() {
  return (
    <div className="App">
      <RouterOutlet />
    </div>
  );
}

export default App;
